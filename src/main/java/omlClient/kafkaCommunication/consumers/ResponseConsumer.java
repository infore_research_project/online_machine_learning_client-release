package omlClient.kafkaCommunication.consumers;

import omlClient.kafkaCommunication.KafkaConstants;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Collections;
import java.util.Properties;

/**
 * A class, wrapping a Kafka response consumer. The class provides
 * helper methods for sending data and managing the Kafka Producer.
 */
public class ResponseConsumer {
    Consumer<String, String> consumer; // The actual Kafka consumer

    public ResponseConsumer() {

    }

    // Main constructor
    public ResponseConsumer(String topic, String broker_list) {

        // Creating the properties of the Response Kafka consumer
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, broker_list);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, KafkaConstants.RESPONSE_GROUP_ID);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        this.consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(topic));
        System.out.println("INFO [KafkaConsumer]: " + this.toString() + " subscribed to topic " + topic);
    }

    /**
     * This method consumes responses from the Online Machine Learning component.
     *
     * @return A set of responses
     */
    public ConsumerRecords<String, String> consumeResponses() {
        return consumer.poll(100);
    }

    /**
     * This method closes the consumer if its not null.
     * Never forget to call this method after you are done
     * this response consumer.
     */
    public void close() {
        if (consumer != null) {
            System.out.println("Terminating the Kafka responses consumer " + this.toString() + ".");
            consumer.close();
        }
    }

    /**
     * A factory method
     *
     * @param topic       The name of this topic.
     * @param broker_list The Kafka broker's address. If Kafka is running in a cluster
     *                    then you can provide comma (,) separated addresses.
     * @return A ResponseConsumer instance
     */
    public ResponseConsumer setConsumer(String topic, String broker_list) {
        return new ResponseConsumer(topic, broker_list);
    }

}
