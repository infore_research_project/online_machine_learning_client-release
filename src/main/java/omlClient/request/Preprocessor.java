package omlClient.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

/**
 * A serializable POJO class representing a preprocessor (e.g. a Polynomial Features preprocessor)
 */
public class Preprocessor extends Transformer {

    public Preprocessor() {
        super();
    }

    public Preprocessor(String name, Map<String, Object> hyperparameters, Map<String, Object> parameters) {
        super(name, hyperparameters, parameters);
    }

    @Override
    public String toString() {
        try {
            return toJsonString();
        } catch (JsonProcessingException e) {
            return "Non printable preprocessor.";
        }
    }

    public String toJsonString() throws JsonProcessingException {
        return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
    }
}
